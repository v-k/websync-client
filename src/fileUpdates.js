const fs = require('fs')
const path = require('path')
const httpClient = require('./httpClient')

function touchOwningDirs(filePath, newTimeStamp) {
  const pathPieces = filePath.split('/')

  while (pathPieces.length > 1) {
    console.info('Setting timestamp on ', path.join(...pathPieces))
    fs.utimesSync(
      path.join(...pathPieces),
      newTimeStamp,
      newTimeStamp
    )

    pathPieces.pop()
  }
}

async function downloadFile(resource) {
  const serverPath = encodeURI(resource)
  const localPath = decodeURI(path.join('data', serverPath))
  const localDir = localPath
    .split('/')
    .slice(0, -1)
    .join('/')

  try {
    const stat = fs.statSync(localPath)

    const result = await httpClient.head(serverPath)
    if (result.headers && result.headers.etag == stat.mtimeMs) {
      console.log(
        'We have latest version already, skipping download.',
        resource
      )
      return
    }
  } catch {
    // Doesn't exist or could not retrieve timestamp, we just continue and download.
  }

  fs.mkdirSync(localDir, {
    recursive: true,
  })

  const response = await httpClient.get(serverPath, {
    responseType: 'stream',
  })

  const writeStream = fs.createWriteStream(localPath)

  writeStream.on('finish', () => {
    console.log('Finished downloading file', serverPath, response.headers)
    if (response.headers && response.headers['last-modified']) {
      touchOwningDirs(localPath, new Date(response.headers['last-modified']))
    }
    else {
      console.error('Server did not send last-modified, this can lead to serious sync issues.')
    }
  })

  response.data.on('error', e => {
    console.error('Error downloading file', e)
  })

  response.data.pipe(writeStream)
}

function deleteFile(resource) {
  const serverPath = encodeURI(resource)
  const localPath = decodeURI(path.join('data', serverPath))

  try {
    fs.unlinkSync(localPath)
    try {
      fs.unlinkSync(localPath + '(CasingConflict)')
      console.log('Deleted file casing conflict file')
    } catch {}

    console.log('Deleted file', serverPath)
  } catch {
    console.info('Cannot delete file, probably already gone', serverPath)
  }
}

module.exports = {
  downloadFile,
  deleteFile,
}
