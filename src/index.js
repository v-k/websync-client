const fs = require('fs')
const rimraf = require('rimraf')
const socketio = require('socket.io-client')
const { downloadFile, deleteFile } = require('./fileUpdates')
const { syncContent, waitForPendingDownloads } = require('./syncContent')
const handleLocalChanges = require("./handleLocalChanges");

async function main() {
  const rootFolder = './data'

  const io = socketio('http://localhost:3000', {
    path: '/websync.io',
  })

  io.on('update', update => {
    console.log('Received file update', update)
    switch (update.event) {
      case 'update':
        downloadFile(update.resource)
          .catch(e => {
            console.error('ERROR downloading file, probably out of sync now', e)
          })

        break

      case 'delete':
        return deleteFile(update.resource)
    }
  })

  rimraf.sync(rootFolder)
  await syncContent(rootFolder)
  await waitForPendingDownloads()

  fs.watch(rootFolder, { recursive: true }, (eventType, fileName) => {
    if (eventType === 'rename' || eventType === 'change') {
      handleLocalChanges(rootFolder, fileName).catch(e => {
        console.error('Could not handle local change', e)
      })
    } else {
      console.error("UNIMPLEMENTED FS EVENT", eventType, fileName)
    }
  })
}

main().catch(err => {
  console.error('Unexpected error:', err)
})
