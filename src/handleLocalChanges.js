const fs = require('fs')
const path = require('path')
const httpClient = require('./httpClient')

async function uploadFile(localFileName, remoteUri) {
  const readStream = fs.createReadStream(localFileName)

  readStream.on('end', () => {
    console.log('upload done', remoteUri)
  })

  const res = await httpClient.put(remoteUri, readStream)
  if (res.status === 200 || res.status === 201) {
    console.info('Stored successfully')

    if (res.headers['stored-with-timestamp']) {
      const timestamp = Number(res.headers['stored-with-timestamp']) / 1000
      fs.utimesSync(localFileName, timestamp, timestamp)
      console.log('Set timestamp to', timestamp)
    }
  } else {
    throw res
  }
}

async function deleteRemoteFile(remoteUri) {
  try {
    const res = await httpClient.delete(remoteUri)
    if (res.status === 200) {
      console.info('Deleted remote file successfully', remoteUri)
    } else {
      throw res
    }
  } catch (e) {
    if (e.response && e.response.status === 404) {
      console.info('File already gone, nothing else to do.')
    } else {
      throw e
    }
  }
}

async function handleLocalChanges(rootFolder, fileName) {
  const localFileName = path.join(rootFolder, fileName)

  let stat
  try {
    stat = fs.statSync(localFileName)
  } catch (e) {
    if (e.code === 'ENOENT') {
      console.info('File does not exist, need to delete on the server too', localFileName)
      await deleteRemoteFile(fileName)
    }

    return
  }

  if (stat.isFile()) {
    try {
      const result = await httpClient.head(encodeURI(fileName))

      if (result.headers.etag == stat.mtimeMs) {
        console.info(
          'We have latest version already, skipping update.',
          fileName
        )
      }
      else {
        console.info('Server has different version and we have just changed locally. Will upload', fileName)
        await uploadFile(localFileName, fileName)
      }
    } catch (e) {
      if (e.response && e.response.status && e.response.status === 404) {
        console.info('File does not exist on server, need to upload.', fileName)
        await uploadFile(localFileName, fileName)
      } else {
        throw e
      }
    }
  } else {
    console.info('Ignoring update for local non-file', fileName)
  }
}

module.exports = handleLocalChanges
