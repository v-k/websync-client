const fs = require('fs')
const path = require('path')
const httpClient = require('./httpClient')

const pendingDownloads = []
async function waitForPendingDownloads() {
  await Promise.all(pendingDownloads)
}

async function downloadFile(
  serverPath,
  encodedItemName,
  localFileName,
  item,
  dirLastModified,
  rootDir
) {
  const response = await httpClient.get(
    path.join(serverPath, encodedItemName),
    {
      responseType: 'stream',
    }
  )

  try {
    fs.accessSync(localFileName)
    localFileName = localFileName + '(CasingConflict)'
  } catch {
    // Doesn't exist, we're OK
  }

  pendingDownloads.push(
    new Promise((resolve, reject) => {
      const writeStream = fs.createWriteStream(localFileName)
      response.data.pipe(writeStream)

      writeStream.on('finish', () => {
        const date = Number(item.etag) / 1000

        fs.utimesSync(localFileName, date, date)
        if (dirLastModified) {
          fs.utimesSync(rootDir, dirLastModified, dirLastModified)
        }

        resolve()
      })

      response.data.on('error', e => {
        reject(e)
      })
    })
  )
  return localFileName
}

async function syncContent(
  rootDir,
  serverPath = '/',
  dirLastModified = undefined
) {
  fs.mkdirSync(rootDir)

  const response = await httpClient.get(serverPath)
  if (!response.data.content) {
    throw new Error('Response invalid, no content in directory listing')
  }

  for (let item of response.data.content) {
    const encodedItemName = encodeURIComponent(item.name)
    let localFileName = decodeURI(path.join(rootDir, encodedItemName))

    if (item.type === 'collection') {
      await syncContent(
        localFileName,
        path.join(serverPath, encodedItemName),
        new Date(item.lastModified)
      )
    } else if (item.type === 'file') {
      localFileName = await downloadFile(
        serverPath,
        encodedItemName,
        localFileName,
        item,
        dirLastModified,
        rootDir
      )
    }
  }
}

module.exports = {
  syncContent,
  waitForPendingDownloads,
}
