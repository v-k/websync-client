const axios = require('axios')

module.exports = axios.create({
  baseURL: 'http://127.0.0.1:3000/',
  timeout: 1000,
  responseType: 'string',
  headers: {'X-Custom-Header': 'foobar'}
});
